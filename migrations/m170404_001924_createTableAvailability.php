<?php

use yii\db\Migration;

class m170404_001924_createTableAvailability extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%availability}}', [
            'id' => $this->bigPrimaryKey(),
            'days' => $this->string(255)->notNull(),
            'hour_start' => $this->date(),
            'hour_end' => $this->date(),
            'matter_id' => $this->bigInteger(),
            'user_id' => $this->bigInteger(),
            $this->foreignKey('matter_id' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE')
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%availability}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

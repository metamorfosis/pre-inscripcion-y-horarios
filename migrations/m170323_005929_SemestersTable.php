<?php

use yii\db\Migration;

class m170323_005929_SemestersTable extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%semesters}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull(),
            'career_id' => $this->bigInteger()->notNull(),
            'pensum_id' => $this->bigInteger()->notNull(),
            'matter' => $this->bigInteger(255)->notNull(),
            'matter1' => $this->bigInteger(255)->defaultValue(1),
            'matter2' => $this->bigInteger(255)->defaultValue(1),
            'matter3' => $this->bigInteger(255)->defaultValue(1),
            'matter4' => $this->bigInteger(255)->defaultValue(1),
            'matter5' => $this->bigInteger(255)->defaultValue(1),
            'matter6' => $this->bigInteger(255)->defaultValue(1),
            'matter7' => $this->bigInteger(255)->defaultValue(1),
            'matter8' => $this->bigInteger(255)->defaultValue(1),
            'matter9' => $this->bigInteger(255)->defaultValue(1),
            $this->foreignKey('career_id' ,'{{%careers}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('pensum_id' ,'{{%pensums}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('matter','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter1','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter2','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter3','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter4','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter5','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter6','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter7','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter8','{{%matters}}','id','CASCADE','CASCADE'),
            $this->foreignKey('matter9','{{%matters}}','id','CASCADE','CASCADE'),
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%semesters}}',
            ['id', 'name', 'career_id', 'pensum_id','matter', 'matter1', 'matter2', 'matter3', 'matter4', 'matter5', 'matter6', 'matter7', 'matter8', 'matter9'], [
            ['1', '1° Semestre', '1', '1', '3', '2', '4', '5', '6', '7', '8', '9', '1', '1'],
            ['2', '2° Semestre', '1', '1', '10', '11', '12', '13', '14', '15', '16', '1', '1', '1'],
            ['3', '3° Semestre', '1', '1', '17', '18', '19', '20', '21', '37', '1', '1', '1', '1'],
            ['4', '4° Semestre', '1', '1', '23', '24', '25', '26', '27', '59', '28', '1', '1', '1'],
            ['5', '5° Semestre', '1', '1', '29', '30', '31', '32', '33', '34', '35', '1', '1', '1'],
            ['6', '6° Semestre', '1', '1', '36', '37', '38', '39', '40', '41', '42', '1', '1', '1'],
            ['7', '7° Semestre', '1', '1', '43', '44', '45', '46', '47', '48', '49', '50', '1', '1'],
            ['8', '8° Semestre', '1', '1', '51', '52', '53', '54', '55', '56', '57', '1', '1', '1'],
            ['9', 'Pasantias', '1', '1', '58', '1', '1', '1', '1', '1', '1', '1', '1', '1'],
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%semesters}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

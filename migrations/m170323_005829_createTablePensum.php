<?php

use yii\db\Migration;

class m170323_005829_createTablePensum extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pensums}}', [
            'id' => $this->bigPrimaryKey(),
            'year' => $this->integer()->notNull(),
            'turn' => $this->string()->notNull(),
            'career_id' => $this->bigInteger()->notNull(),
            $this->foreignKey('career_id' ,'{{%careers}}', 'id', 'CASCADE','CASCADE')
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%pensums}}', ['year', 'turn', 'career_id'], [
            ['2009', 'Diurno', '1']
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%pensums}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

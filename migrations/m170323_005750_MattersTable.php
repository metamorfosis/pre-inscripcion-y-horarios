<?php

use yii\db\Migration;

class m170323_005750_MattersTable extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%matters}}', [
            'id' => $this->bigPrimaryKey(),
            'cod' => $this->string(255)->notNull(),
            'name' => $this->string(255)->notNull(),
            'priority1' => $this->string(255)->notNull(),
            'priority2' => $this->string(255)->defaultValue(NULL),
            'unitsCredit' => $this->integer()->notNull(),
            'hoursLab' => $this->integer(255)->defaultValue(0),
            'hoursTheory' => $this->integer(255)->notNull(),
            'hoursPractice' => $this->integer(255)->defaultValue(0),
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%matters}}',
            ['id', 'cod', 'name', 'priority1', 'priority2', 'unitsCredit', 'hoursLab', 'hoursTheory', 'hoursPractice'], [
                ['1', '', 'N/A', '', null, '0', null, '0', null],
                ['2', 'ADG-25123', 'Hombre, Sociedad, Ciencia y Tecnologia', 'N/A', '', '3', '0', '2', '2'],
                ['3', 'ADG-25132', 'Educación Ambiental', 'N/A', '', '2', '0', '2', '1'],
                ['4', 'IDM-24113', 'Ingles I', 'N/A', '', '3', '0', '2', '2'],
                ['5', 'MAT-21212', 'Dibujo', 'N/A', '', '2', '0', '1', '3'],
                ['6', 'MAT-21215', 'Matemática I', 'N/A', '', '5', '0', '4', '2'],
                ['7', 'MAT-21524', 'Geometría Analítica', 'N/A', '', '4', '0', '3', '2'],
                ['8', 'ADG-25131', 'Seminario I', 'N/A', '', '1', '0', '1', '0'],
                ['9', 'DIN-21113', 'Defensa Integral de la Nación I', 'N/A', '', '3', '0', '2', '2'],
                ['10', 'IDM-24123', 'Ingles II', 'IDM-24113', '', '3', '0', '2', '2'],
                ['11', 'QUF-22014', 'Quimica General', 'N/A', '', '4', '3', '2', '2'],
                ['12', 'QUF-23015', 'Física I', 'MAT-21215', 'MAT-21524', '5', '2', '4', '2'],
                ['13', 'MAT-21225', 'Matemática II', 'MAT-21215', 'MAT-21524', '5', '0', '4', '2'],
                ['14', 'MAT-21114', 'Álgebra Lineal', 'MAT-21215', 'MAT-21524', '4', '0', '2', '4'],
                ['15', 'ADG-25141', 'Seminario II', 'ADG-25131', '', '1', '0', '1', '0'],
                ['16', 'DIN-21123', 'Defensa Integral de la Nación II', 'DIN-21113', '', '3', '0', '2', '2'],
                ['17', 'QUF-23025', 'Física II', 'QUF-23015', 'MAT-21225', '5', '2', '4', '2'],
                ['18', 'MAT-21235', 'Matemática III', 'MAT-21225', '', '5', '0', '4', '2'],
                ['19', 'MAT-21414', 'Probabilidad y Estadística', 'MAT-21225', '', '4', '0', '2', '4'],
                ['20', 'SYC-22113', 'Programación', 'MAT-21114', '', '3', '3', '2', '0'],
                ['21', 'AGG-22313', 'Sistemas Administrativos', 'N/A', '', '4', '0', '3', '3'],
                ['22', 'DIN-21133', 'Defensa Integral de la Nación III', 'DIN-21123', '', '3', '0', '2', '2'],
                ['23', 'SYC-32114', 'Teoría de los Sistemas', 'N/A', '', '4', null, '3', '3'],
                ['24', 'MAT-31714', 'Cálculo Numérico', 'MAT-21235', '', '4', null, '3', '3'],
                ['25', 'MAT-31214', 'Lógica Matemática', 'MAT-21114', '', '4', null, '3', '3'],
                ['26', 'SYC-32225', 'Lenguaje de Programación I', 'SYC-22113', '', '5', '3', '4', null],
                ['27', 'SYC-32414', 'Procesamiento de Datos', 'SYC-22113', '', '4', '3', '3', null],
                ['28', 'DIN-31143', 'Defensa Integral de la Nación IV', 'DIN-21133', '', '3', null, '2', '2'],
                ['29', 'MAT-31114', 'Teoría de Grafos', 'MAT-31213', 'MAT-21413', '4', null, '3', '3'],
                ['30', 'SYC-32235', 'Lenguaje de Programación II', 'SYC-32225', '', '5', '3', '4', null],
                ['31', 'MAT-30925', 'Investigación de Operaciones', 'MAT-31714', '', '5', null, '4', '3'],
                ['32', 'ELN-30514', 'Circuitos Logícos', 'MAT-31214', '', '4', null, '3', '2'],
                ['33', 'SYC-32514', 'Análisis de Sistemas', 'SYC-32114', '', '4', null, '3', '3'],
                ['34', 'syc-32614', 'Base de Datos', 'SYC-32114', '', '4', '3', '3', null],
                ['35', 'DIN-31153', 'Defensa Integral de la Nación V', 'DIN-31143', '', '3', null, '2', '2'],
                ['36', 'MAT-30935', 'Optimización No Linea ', 'MAT-30925', '', '5', null, '4', '3'],
                ['37', 'SYC-32245', 'Lenguaje de Programación III', 'SYC-32235', '', '5', '3', '4', null],
                ['38', 'MAT-31414', 'Procesos Estocastícos', 'MAT-30925', 'MAT-31114', '4', null, '3', '3'],
                ['39', 'SYC-30525', 'Arquitectura del Computador', 'ELN-30514', '', '5', null, '4', '3'],
                ['40', 'SYC-32524', 'Diseño de Sistemas', 'SYC-32514', '', '4', null, '3', '3'],
                ['41', 'SYC-30834', 'Sistemas Operativos', 'SYC-30525', '', '4', '3', '3', null],
                ['42', 'DIN-31163', 'Defensa Integral de la Nación VI', 'DIN-31153', '', '3', null, '2', '2'],
                ['43', 'SYC-32714', 'Implantación de Sistemas', 'SYC-32524', '', '4', null, '3', '3'],
                ['44', 'ADG-30214', 'Metodología de la Investigación', 'N/A', '', '4', null, '3', '2'],
                ['45', 'MAT-30945', 'Simulación y Modelos', 'MAT-30935', 'MAT-31414', '5', '3', '4', null],
                ['46', 'SYC-31644', 'Redes', 'SYC-30834', '', '4', '3', '3', null],
                ['47', 'ADG-30224', 'Gerencia de la Informatica', 'N/A', '', '4', null, '3', '2'],
                ['48', 'ET-7', 'Electiva Tecnica', 'N/A', '', '3', null, '3', null],
                ['49', 'ENT-7', 'Electiva No Tecnica', 'N/A', '', '3', null, '3', null],
                ['50', 'DIN-31173', 'Defensa Integral de la Nación VII', 'DIN-31163', '', '3', null, '2', '2'],
                ['51', 'MAT-31314', 'Teoría de Decisiones', 'MAT-30945', '', '4', null, '3', '2'],
                ['52', 'SYC-32814', 'Auditoria de SIstemas', 'SYC-32714', '', '4', null, '3', '3'],
                ['53', 'CJU-37314', 'Marco Legal Para el Ejercicio de la Ingenieria', 'N/A', '', '4', null, '4', null],
                ['54', 'TTC-31154', 'Teleprocesos', 'SYC-31644', '', '4', null, '3', '3'],
                ['55', 'ET-8', 'Electiva Tecnica', 'N/A', '', '3', null, '3', null],
                ['56', 'ENT-8', 'Electiva No Tecnica', 'N/A', '', '3', null, '3', null],
                ['57', 'DIN-31183', 'Defensa Integral de la Nación VIII', 'DIN-31173', '', '3', null, '2', '2'],
                ['58', 'TGR-30010', 'Trabajo Especial de Grado', 'N/A', '', '10', null, '0', null],
                ['59', 'AGL-30214', 'Sistemas de Produccion', 'AGG-22313', '', '4', null, '3', '3'],
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%matters}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

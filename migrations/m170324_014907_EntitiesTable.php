<?php

use yii\db\Migration;

class m170324_014907_EntitiesTable extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%entities}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(255),
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%entities}}', ['id', 'name', 'description'], [
            ['1', 'Lunes', ''],
            ['2', 'Martes', ''],
            ['3', 'Miercoles', ''],
            ['4', 'Jueves', ''],
            ['5', 'Viernes', ''],
            ['6', 'Sabado', ''],
            ['7', 'Domingo', ''],
            ['8', '7:30', 'block1'],
            ['9', '8:15', 'block2'],
            ['10', '9:00', 'block3'],
            ['11', '9:45', 'block4'],
            ['12', '10:30', 'block5'],
            ['13', '11:15', 'block6'],
            ['14', '12:00', 'block7'],
            ['15', '1:00', 'block8'],
            ['16', '1:45', 'block9'],
            ['17', '2:30', 'block10'],
            ['18', '3:15', 'block11'],
            ['19', '4:00', 'block12'],
            ['20', '4:45', 'block13'],
            ['21', '5:30', 'block14']
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%entities}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170404_020112_createTableScheduleDetails extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%scheduleDetails}}', [
            'id' => $this->bigPrimaryKey(),
            'schedule_id' => $this->bigInteger()->notNull(),
            'day' => $this->smallInteger()->notNull(),
            'block1' => $this->bigInteger()->notNull(),
            'block2' => $this->bigInteger()->notNull(),
            'block3' => $this->bigInteger()->notNull(),
            'block4' => $this->bigInteger()->notNull(),
            'block5' => $this->bigInteger()->notNull(),
            'block6' => $this->bigInteger()->notNull(),
            'block7' => $this->bigInteger()->notNull(),
            'block8' => $this->bigInteger()->notNull(),
            'block9' => $this->bigInteger()->notNull(),
            'block10' => $this->bigInteger()->notNull(),
            'block11' => $this->bigInteger()->notNull(),
            'block12' => $this->bigInteger()->notNull(),
            'block13' => $this->bigInteger()->notNull(),
            'user1_id' => $this->bigInteger()->notNull(),
            'user2_id' => $this->bigInteger()->notNull(),
            'user3_id' => $this->bigInteger()->notNull(),
            'user4_id' => $this->bigInteger()->notNull(),
            'user5_id' => $this->bigInteger()->notNull(),
            'user6_id' => $this->bigInteger()->notNull(),
            'user7_id' => $this->bigInteger()->notNull(),
            'user8_id' => $this->bigInteger()->notNull(),
            'user9_id' => $this->bigInteger()->notNull(),
            'user10_id' => $this->bigInteger()->notNull(),
            'user11_id' => $this->bigInteger()->notNull(),
            'user12_id' => $this->bigInteger()->notNull(),
            'user13_id' => $this->bigInteger()->notNull(),
            $this->foreignKey('block1' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block2' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block3' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block4' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block5' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block6' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block7' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block8' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block9' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block10' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block11' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block12' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('block13' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user1_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user2_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user3_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user4_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user5_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user6_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user7_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user8_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user9_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user10_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user11_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user12_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('user13_id' ,'{{%users}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('schedule_id' ,'{{%schedules}}', 'id', 'CASCADE','CASCADE'),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%scheduleDetails}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

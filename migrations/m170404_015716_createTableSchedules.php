<?php

use yii\db\Migration;

class m170404_015716_createTableSchedules extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%schedules}}', [
            'id' => $this->bigPrimaryKey(),
            'classroom_id' => $this->bigInteger()->notNull(),
            'semester_id' => $this->bigInteger()->notNull(),
            $this->foreignKey('classroom_id' ,'{{%classrooms}}', 'id', 'CASCADE','CASCADE'),
            $this->foreignKey('semester_id' ,'{{%semesters}}', 'id', 'CASCADE','CASCADE')
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%schedules}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170323_045824_UsersTable extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }



    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}',
            [
                'id' => $this->bigPrimaryKey(),
                'ci' => $this->integer(),
                'name' => $this->string(255)->notNull(),
                'last_name' => $this->string(255)->notNull(),
                'birthdate' => $this->date()->notNull(),
                'address' => $this->string(1500)->notNull(),
                'movil' => $this->string(15)->notNull(),
                'type' => $this->string(15)->notNull(),
                'matter_id' => $this->bigInteger()->defaultValue(NULL),
                'pensum_id' => $this->bigInteger()->defaultValue(NULL),
                'username' => $this->string()->notNull()->unique(),
                'email' => $this->string(255)->notNull()->unique(),
                'auth_key' => $this->string(32)->notNull(),
                'accessToken' => $this->string(32)->notNull(),
                'password_hash' => $this->string()->notNull(),
                'password_reset_token' => $this->string()->unique(),
                'status_id' => $this->bigInteger()->notNull()->defaultValue(3),
                'created_at' => $this->date()->notNull(),
                'updated_at' => $this->date()->notNull(),
                $this->foreignKey('matter_id' ,'{{%matters}}', 'id', 'CASCADE','CASCADE'),
                $this->foreignKey('pensum_id' ,'{{%pensums}}', 'id', 'CASCADE','CASCADE'),
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%users}}',
            [
                'ci', 'name', 'last_name', 'birthdate', 'address', 'movil', 'type', 'matter_id',
                'username', 'email', 'auth_key', 'accessToken', 'password_hash', 'password_reset_token', 'status_id', 'created_at', 'updated_at' ],
            [
                [
                    '18277864', 'Jose', 'Guerrero', '2016-05-20', 'por aki', '04263412354', 'Admin', NULL,
                    'morogasper', 'joseaguerreroh@gmail.com', Yii::$app->security->generateRandomString(), Yii::$app->security->generateRandomString(),
                    'e10adc3949ba59abbe56e057f20f883e', NULL, '1', '2016-05-20', '2016-05-20'
                ],
            ]);
    }

    public function down()
    {
        $this->dropTable('{{%users}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

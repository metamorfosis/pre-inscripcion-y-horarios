<?php

use yii\db\Migration;

class m170324_005656_ClassroomTable extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%classrooms}}', [
            'id' => $this->bigPrimaryKey(),
            'cod' => $this->string(255)->notNull(),
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%classrooms}}', ['id', 'cod'], [
            ['1', '1001'],
            ['2', '1002'],
            ['3', '1003'],
            ['4', '1004'],
            ['5', '1005'],
            ['6', '1006'],
            ['7', '1007'],
            ['8', '1008'],
            ['9', '1009'],
            ['10', '1010'],
            ['11', '1011'],
            ['12', '1012'],
            ['13', '1013'],
            ['14', '1014'],
            ['15', '1015'],
            ['16', '1016'],
            ['17', '1017'],
            ['18', '1018'],
            ['19', '1019'],
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%classrooms}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m170403_001814_createTableAuthItem extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function primaryKeys($columns) {
        return 'PRIMARY KEY (' . $this->db->getQueryBuilder()->buildColumns($columns) . ')';
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%AuthItem}}', [
            'name' => $this->string()->notNull(),
            'type' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
            $this->primaryKeys(['name'])
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%AuthItem}}', ['name', 'type', 'description', 'bizrule', 'data'], [
            ['Admin', '2', 'Administrador de la Aplicación', null, 'N;'],
            ['Coordinador', '2', 'Coordinador de Carrera', null, 'N;'],
            ['Estudiante', '2', 'Estudiante de la UNEFA', null, 'N;'],
            ['Profesor', '2', 'Profesor de la UNEFA', null, 'N;'],
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%AuthItem}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

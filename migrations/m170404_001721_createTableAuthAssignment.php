<?php

use yii\db\Migration;

class m170404_001721_createTableAuthAssignment extends Migration
{
    public function mysql($yes,$no='') {
        return $this->db->driverName === 'mysql' ? $yes : $no;
    }

    public function primaryKeys($columns) {
        return 'PRIMARY KEY (' . $this->db->getQueryBuilder()->buildColumns($columns) . ')';
    }

    public function foreignKey($columns,$refTable,$refColumns,$onDelete = null,$onUpdate = null) {
        $builder = $this->db->getQueryBuilder();
        $sql = ' FOREIGN KEY (' . $builder->buildColumns($columns) . ')'
            . ' REFERENCES ' . $this->db->quoteTableName($refTable)
            . ' (' . $builder->buildColumns($refColumns) . ')';
        if ($onDelete !== null) {
            $sql .= ' ON DELETE ' . $onDelete;
        }
        if ($onUpdate !== null) {
            $sql .= ' ON UPDATE ' . $onUpdate;
        }
        return $sql;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%AuthAssignment}}', [
            'itemname' => $this->string()->notNull(),
            'userid' => $this->bigInteger()->notNull(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
            $this->primaryKeys('userid'),
            $this->foreignKey('itemname' ,'{{%AuthItem}}', 'name', 'CASCADE','CASCADE')
        ], $tableOptions);

        // Insert Data
        $this->batchInsert('{{%AuthAssignment}}', ['itemname', 'userid', 'bizrule', 'data'], [
            ['Admin', '1', null, 'N;'],
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%AuthAssignment}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
